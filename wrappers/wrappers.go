package wrappers

import (
	"errors"
	"gitee.com/hongzhaomin/hzm-common-go/strutil"
	"gitee.com/hongzhaomin/hzm-common-go/toolkit"
	"gitee.com/hongzhaomin/xormplus/base"
	"reflect"
	"strings"
	"sync"
	"xorm.io/builder"
	"xorm.io/xorm"
)

type Wrapper[Entity base.Domain] struct {
	entity   Entity
	selects  []string
	once     *sync.Once
	bd       *builder.Builder
	isOr     bool
	cond     builder.Cond
	orderBy  builder.Cond
	groupBys []string
	having   string
	limit    int
	start    int
}

func Query[Entity base.Domain]() *Wrapper[Entity] {
	entity := new(Entity)
	if reflect.TypeOf(entity).Elem().Kind() != reflect.Struct {
		panic(errors.New("必须为结构体"))
	}
	return &Wrapper[Entity]{
		entity: *entity,
		once:   new(sync.Once),
		bd:     builder.MySQL(),
		isOr:   false,
		cond:   builder.NewCond(),
	}
}

func (wp *Wrapper[Entity]) PutCond(cond builder.Cond) *Wrapper[Entity] {
	return wp.PutCondIsTrue(true, cond)
}

func (wp *Wrapper[Entity]) PutCondIsTrue(condition bool, cond builder.Cond) *Wrapper[Entity] {
	if condition {
		if wp.isOr {
			wp.cond = wp.cond.Or(cond)
			wp.isOr = false
		} else {
			wp.cond = wp.cond.And(cond)
		}
	}
	return wp
}

func (wp *Wrapper[Entity]) build(engine *xorm.Engine) {
	rt := reflect.TypeOf(wp.entity)
	tableName := engine.GetTableMapper().Obj2Table(rt.Name())
	if len(wp.selects) > 0 {
		wp.bd.Select(wp.selects...)
	} else {
		wp.bd.Select(getColumnNames(wp.entity, engine.GetColumnMapper().Obj2Table)...)
	}
	wp.bd.From(tableName)
	wp.bd.Where(wp.cond)
	if len(wp.groupBys) > 0 {
		wp.bd.GroupBy(strings.Join(wp.groupBys, strutil.Comma))
	}
	if strutil.IsNotBlank(wp.having) {
		wp.bd.Having(wp.having)
	}
	if wp.orderBy != nil {
		wp.bd.OrderBy(wp.orderBy)
	}
	if wp.limit > 0 {
		wp.bd.Limit(wp.limit, wp.start)
	}
}

func getColumnNames(structVal any, convertName func(fileName string) string) []string {
	rt := reflect.TypeOf(structVal)
	n := rt.NumField()
	forceExcludeFields := make([]string, 0)
	for i := 0; i < n; i++ {
		field := rt.Field(i)
		if field.Anonymous {
			// 强制排除内嵌字段
			forceExcludeFields = append(forceExcludeFields, field.Name)
		}
	}
	return toolkit.GetAllFieldNames4CustomStrategy(structVal, convertName, forceExcludeFields...)
}

func (wp *Wrapper[Entity]) GetBuilder(engine *xorm.Engine) *builder.Builder {
	// 只构建一次
	wp.once.Do(func() {
		wp.build(engine)
	})
	return wp.bd
}

func (wp *Wrapper[Entity]) GetCond() builder.Cond {
	return wp.cond
}

func (wp *Wrapper[Entity]) GetSelect() []string {
	return wp.selects
}

func (wp *Wrapper[Entity]) Select(col ...string) *Wrapper[Entity] {
	wp.selects = append(wp.selects, col...)
	return wp
}

func (wp *Wrapper[Entity]) AndSub(andFunc func(subWrapper *Wrapper[Entity])) *Wrapper[Entity] {
	return wp.AndSubIsTrue(true, andFunc)
}

func (wp *Wrapper[Entity]) AndSubIsTrue(condition bool, andFunc func(subWrapper *Wrapper[Entity])) *Wrapper[Entity] {
	if condition {
		subWrapper := Query[Entity]()
		andFunc(subWrapper)
		wp.cond = wp.cond.And(subWrapper.cond)
	}
	return wp
}

func (wp *Wrapper[Entity]) Or() *Wrapper[Entity] {
	wp.isOr = true
	return wp
}

func (wp *Wrapper[Entity]) OrSub(orFunc func(subWrapper *Wrapper[Entity])) *Wrapper[Entity] {
	return wp.OrSubIsTrue(true, orFunc)
}

func (wp *Wrapper[Entity]) OrSubIsTrue(condition bool, orFunc func(subWrapper *Wrapper[Entity])) *Wrapper[Entity] {
	if condition {
		subWrapper := Query[Entity]()
		orFunc(subWrapper)
		wp.cond = wp.cond.Or(subWrapper.cond)
	}
	return wp
}

func (wp *Wrapper[Entity]) Eq(col string, val any) *Wrapper[Entity] {
	return wp.EqIsTrue(true, col, val)
}

func (wp *Wrapper[Entity]) EqIsTrue(condition bool, col string, val any) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.Eq{col: val})
}

func (wp *Wrapper[Entity]) EqMap(eqMap map[string]interface{}) *Wrapper[Entity] {
	return wp.EqMapIsTrue(true, eqMap)
}

// EqMapIsTrue 可以将多个col进行and拼接：col1 = ? and col2 = ?...
// 另外，如果map的val是切片类型，则会使用in查询：col1 = ? and col2 = ? and col3 in(v1, v2 ...)
func (wp *Wrapper[Entity]) EqMapIsTrue(condition bool, eqMap map[string]interface{}) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.Eq(eqMap))
}

func (wp *Wrapper[Entity]) Neq(col string, val any) *Wrapper[Entity] {
	return wp.NeqIsTrue(true, col, val)
}

func (wp *Wrapper[Entity]) NeqIsTrue(condition bool, col string, val any) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.Neq{col: val})
}

func (wp *Wrapper[Entity]) In(col string, vals ...any) *Wrapper[Entity] {
	return wp.InIsTrue(true, col, vals...)
}

// InSql in查询条件可拼接sql
// 例如：builder.In("a", builder.Expr("select id from x where name > ?", "b"))
func (wp *Wrapper[Entity]) InSql(col string, sql string) *Wrapper[Entity] {
	return wp.InIsTrue(true, col, builder.Expr(sql))
}

func (wp *Wrapper[Entity]) InIsTrue(condition bool, col string, vals ...any) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.In(col, vals...))
}

func (wp *Wrapper[Entity]) NotIn(col string, vals ...any) *Wrapper[Entity] {
	return wp.NotInIsTrue(true, col, vals...)
}

func (wp *Wrapper[Entity]) NotInSql(col string, sql string) *Wrapper[Entity] {
	return wp.NotInIsTrue(true, col, builder.Expr(sql))
}

func (wp *Wrapper[Entity]) NotInIsTrue(condition bool, col string, vals ...any) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.NotIn(col, vals...))
}

func (wp *Wrapper[Entity]) Like(col string, val string) *Wrapper[Entity] {
	return wp.LikeIsTrue(true, col, val)
}

func (wp *Wrapper[Entity]) LikeLeft(col string, val string) *Wrapper[Entity] {
	return wp.LikeLeftIsTrue(true, col, val)
}

func (wp *Wrapper[Entity]) LikeRight(col string, val string) *Wrapper[Entity] {
	return wp.LikeRightIsTrue(true, col, val)
}

func (wp *Wrapper[Entity]) LikeIsTrue(condition bool, col string, val string) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.Like{col, val})
}

func (wp *Wrapper[Entity]) LikeLeftIsTrue(condition bool, col string, val string) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.Like{col, "%" + val})
}

func (wp *Wrapper[Entity]) LikeRightIsTrue(condition bool, col string, val string) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.Like{col, val + "%"})
}

func (wp *Wrapper[Entity]) LtIf(col string, val any) *Wrapper[Entity] {
	return wp.LtIsTrue(true, col, val)
}

func (wp *Wrapper[Entity]) LteIf(col string, val any) *Wrapper[Entity] {
	return wp.LteIsTrue(true, col, val)
}

func (wp *Wrapper[Entity]) GtIf(col string, val any) *Wrapper[Entity] {
	return wp.GtIsTrue(true, col, val)
}

func (wp *Wrapper[Entity]) GteIf(col string, val any) *Wrapper[Entity] {
	return wp.GteIsTrue(true, col, val)
}

func (wp *Wrapper[Entity]) LtIsTrue(condition bool, col string, val any) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.Lt{col: val})
}

func (wp *Wrapper[Entity]) LteIsTrue(condition bool, col string, val any) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.Lte{col: val})
}

func (wp *Wrapper[Entity]) GtIsTrue(condition bool, col string, val any) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.Gt{col: val})
}

func (wp *Wrapper[Entity]) GteIsTrue(condition bool, col string, val any) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.Gte{col: val})
}

func (wp *Wrapper[Entity]) IsNull(col string) *Wrapper[Entity] {
	return wp.IsNullIsTrue(true, col)
}

func (wp *Wrapper[Entity]) IsNullIsTrue(condition bool, col string) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.IsNull{col})
}

func (wp *Wrapper[Entity]) NotNull(col string) *Wrapper[Entity] {
	return wp.NotNullIsTrue(true, col)
}

func (wp *Wrapper[Entity]) NotNullIsTrue(condition bool, col string) *Wrapper[Entity] {
	return wp.PutCondIsTrue(condition, builder.NotNull{col})
}

func (wp *Wrapper[Entity]) Limit(limit int, start ...int) *Wrapper[Entity] {
	wp.limit = limit
	if len(start) > 0 {
		wp.start = start[0]
	}
	return wp
}

func (wp *Wrapper[Entity]) GroupBy(cols ...string) *Wrapper[Entity] {
	return wp.GroupByIsTrue(true, cols...)
}

func (wp *Wrapper[Entity]) GroupByIsTrue(condition bool, cols ...string) *Wrapper[Entity] {
	if condition && len(cols) > 0 {
		wp.groupBys = append(wp.groupBys, cols...)
	}
	return wp
}

func (wp *Wrapper[Entity]) Having(sqlHaving string, vals ...string) *Wrapper[Entity] {
	return wp.HavingIsTrue(true, sqlHaving, vals...)
}

// HavingIsTrue ( sql语句 )
// 例1: having("sum(age) > 10 and sum(age) < 30")
// 例2: having("sum(age) > ? and sum(age) < ?", 10, 30)
func (wp *Wrapper[Entity]) HavingIsTrue(condition bool, sqlHaving string, vals ...string) *Wrapper[Entity] {
	if condition && strutil.IsNotBlank(sqlHaving) {
		if len(vals) > 0 {
			expr := builder.Expr(sqlHaving, vals)
			sqlFragments, err := builder.ToBoundSQL(expr)
			if err == nil {
				panic(err)
			}
			wp.having = sqlFragments
		} else {
			wp.having = sqlHaving
		}
	}
	return wp
}

func (wp *Wrapper[Entity]) OrderByAsc(cols ...string) *Wrapper[Entity] {
	return wp.OrderByAscIsTrue(true, cols...)
}

func (wp *Wrapper[Entity]) OrderByDesc(cols ...string) *Wrapper[Entity] {
	return wp.OrderByDescIsTrue(true, cols...)
}

// OrderByAscIsTrue 排序：ORDER BY 字段, ... ASC
// 例: orderByAsc("id", "name")
func (wp *Wrapper[Entity]) OrderByAscIsTrue(condition bool, cols ...string) *Wrapper[Entity] {
	if condition && len(cols) > 0 {
		orderByAsc := []string{
			strings.Join(cols, strutil.Comma),
			base.Asc,
		}
		wp.orderBy = builder.Expr(strings.Join(orderByAsc, strutil.Space))
	}
	return wp
}

func (wp *Wrapper[Entity]) OrderByDescIsTrue(condition bool, cols ...string) *Wrapper[Entity] {
	if condition && len(cols) > 0 {
		orderByDesc := []string{
			strings.Join(cols, strutil.Comma),
			base.Desc,
		}
		wp.orderBy = builder.Expr(strings.Join(orderByDesc, strutil.Space))
	}
	return wp
}
