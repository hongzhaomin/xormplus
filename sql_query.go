package xormplus

import (
	"gitee.com/hongzhaomin/hzm-common-go/streams"
	"gitee.com/hongzhaomin/hzm-common-go/strutil"
	"gitee.com/hongzhaomin/hzm-common-go/toolkit"
	"gitee.com/hongzhaomin/xormplus/base"
)

func Query[Bean comparable](sql string, args ...any) []*Bean {
	return QueryByDataSourceId[Bean](strutil.Empty, sql, args...)
}

func Exec(sql string, args ...any) {
	ExecByDataSourceId(strutil.Empty, sql, args...)
}

// QueryByDataSourceId 对于多数据源，可以指定数据源执行sql
func QueryByDataSourceId[Bean comparable](dataSourceId string, sql string, args ...any) []*Bean {
	if strutil.IsBlank(sql) {
		return nil
	}
	sqlAndArgs := append(args, sql)
	engine := GetDataSourceById(dataSourceId)
	sliceMap, err := engine.QueryInterface(sqlAndArgs...)
	if err != nil {
		base.Logger.Error("QueryByDataSourceId SQL查询失败: %v", err)
		panic(err)
	}
	beanMapsStream := streams.OfSlice(sliceMap).
		MapIdentity(func(tableMap map[string]any) map[string]any {
			beanMap := make(map[string]any, len(tableMap))
			for col, val := range tableMap {
				fieldName := engine.GetColumnMapper().Table2Obj(col)
				beanMap[fieldName] = val
			}
			return beanMap
		})
	return streams.MapStream(beanMapsStream, toolkit.Map2Bean[Bean]).ToSlice()
}

func ExecByDataSourceId(dataSourceId string, sql string, args ...any) {
	if strutil.IsBlank(sql) {
		return
	}
	sqlAndArgs := append(args, sql)
	engine := GetDataSourceById(dataSourceId)
	_, err := engine.Exec(sqlAndArgs...)
	if err != nil {
		base.Logger.Error("ExecByDataSourceId SQL执行失败: %v", err)
		panic(err)
	}
}
