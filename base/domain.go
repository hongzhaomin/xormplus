package base

import "xorm.io/builder"

type Domain interface {

	// GetId 可以通过泛型类型和泛型实例获取id，这在java中要简单很多，直接entity.GetId()即可
	// 通常有两种方式：
	//		1、通过泛型参数Model.GetId(instance)
	// 		2、通过使用base.Domain.GetId(instance)
	// 注意：参数instance需要传实例对象或实例对象的指针，具体传啥需要看定义的接收者是对象还是对象指针
	GetId() int64

	// IsBuiltin 用于判断是否是Entity类型，因为go和Java不一样，继承的父类类型不能作为子类实例的接受类型
	IsBuiltin() bool

	// DefaultAndCond4Select 默认为查询语句添加的条件
	DefaultAndCond4Select() builder.Cond
}
