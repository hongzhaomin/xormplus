package base

import (
	"gitee.com/hongzhaomin/hzm-common-go/easylog"
	"log/slog"
)

const (
	IdColumnName = "id"
	Desc         = "DESC"
	Asc          = "ASC"
)

var Logger = easylog.NewDefaultLogger(slog.LevelInfo)
