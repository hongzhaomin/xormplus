package xormplus

import (
	"errors"
	"gitee.com/hongzhaomin/hzm-common-go/assert"
	"gitee.com/hongzhaomin/hzm-common-go/concurrent"
	"gitee.com/hongzhaomin/hzm-common-go/strutil"
	"xorm.io/xorm"
)

type DataSourceManager struct {
	engineMap *concurrent.Map[string, *xorm.Engine]
}

func (dsm *DataSourceManager) Regis(dataSourceId string, engine *xorm.Engine) {
	//base.Logger.Info("Starting regis data source: %s", dataSourceId)
	assert.IsTrue(strutil.IsNotBlank(dataSourceId), "dataSourceId must not blank")
	assert.NonNil(engine, "engine must not nil")
	dsm.engineMap.Store(dataSourceId, engine)
	//base.Logger.Info("Data source [%s] registered success", dataSourceId)
}

func (dsm *DataSourceManager) GetById(dataSourceId string) *xorm.Engine {
	if strutil.IsBlank(dataSourceId) && dsm.engineMap.Size() == 1 {
		// 如果 dataSourceId 为空，且 engine 只有一个，直接取出它即可
		var engine *xorm.Engine
		dsm.engineMap.Range(func(key string, value *xorm.Engine) (shouldContinue bool) {
			engine = value
			return false
		})
		return engine
	}
	if engine, ok := dsm.engineMap.Load(dataSourceId); ok {
		return engine
	}
	panic(errors.New("not found dataSource, please regis it"))
}

var dataSourceManager = DataSourceManager{
	engineMap: concurrent.NewMap[string, *xorm.Engine](),
}

func RegisDataSources(dataSourceId string, engine *xorm.Engine) {
	dataSourceManager.Regis(dataSourceId, engine)
}

func GetDataSourceById(dataSourceId string) *xorm.Engine {
	return dataSourceManager.GetById(dataSourceId)
}

func GetDataSource() *xorm.Engine {
	return GetDataSourceById(strutil.Empty)
}
