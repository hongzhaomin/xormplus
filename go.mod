module gitee.com/hongzhaomin/xormplus

go 1.20

require (
	gitee.com/hongzhaomin/hzm-common-go v0.0.0-20250312113147-2ebc1ada4653
	xorm.io/builder v0.3.13
	xorm.io/xorm v1.3.9
)

require (
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/goccy/go-json v0.8.1 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
)
