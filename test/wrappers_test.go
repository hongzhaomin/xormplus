package test

import (
	"fmt"
	"gitee.com/hongzhaomin/xormplus"
	"gitee.com/hongzhaomin/xormplus/wrappers"
	"testing"
	"xorm.io/builder"
)

func TestWrappers(t *testing.T) {
	cond := wrappers.Query[xormplus.Entity]().Eq("valid", 1).GetCond()
	sql, err := builder.ToBoundSQL(cond)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(sql)
}
