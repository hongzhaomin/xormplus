package test

import (
	"fmt"
	"gitee.com/hongzhaomin/xormplus"
	"gitee.com/hongzhaomin/xormplus/base"
	"testing"
)

type User struct {
	xormplus.Entity
	Name string
}

func (u User) IsBuiltin() bool {
	return false
}

type dao[T base.Domain] struct {
}

func (d dao[T]) name() {
	t := new(T)
	builtin := T.IsBuiltin(*t)
	isBuiltin := base.Domain.IsBuiltin(*t)
	fmt.Println(builtin)
	fmt.Println(isBuiltin)
}

func TestGeneric(t *testing.T) {
	d := new(dao[User])
	d.name()
}
