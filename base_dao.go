package xormplus

import (
	"errors"
	"gitee.com/hongzhaomin/hzm-common-go/strutil"
	"gitee.com/hongzhaomin/hzm-common-go/toolkit"
	"gitee.com/hongzhaomin/xormplus/base"
	"gitee.com/hongzhaomin/xormplus/wrappers"
	"xorm.io/builder"
)

type Dao interface {
	GetDataSourceId() string
}

var _ Dao = (*BaseDao[base.Domain])(nil)

type BaseDao[Model base.Domain] struct{}

func (bd *BaseDao[Model]) GetDataSourceId() string {
	return strutil.Empty
}

// ================================= 新增 =================================

// Save 单条新增，会自动忽略空值字段，forceUpdateCols表示强制插入字段，如果是主键为零值，会自动更新数据库主键值
func (bd *BaseDao[Model]) Save(model *Model, forceUpdateCols ...string) {
	if model != nil {
		engine := GetDataSourceById(bd.GetDataSourceId())
		forceUpdateCols = append(forceUpdateCols, base.IdColumnName)
		omitCols := toolkit.GetZeroFieldNames4CustomStrategy(model,
			engine.GetColumnMapper().Obj2Table, forceUpdateCols...)
		_, err := engine.Omit(omitCols...).Insert(model)
		if err != nil {
			base.Logger.Error("新增失败: %v", err)
			panic(errors.New("新增失败"))
		}
	}
}

// SaveBatch 自动主键赋值批量插入方式，实际是一条一条插入，性能可能会稍有影响
func (bd *BaseDao[Model]) SaveBatch(models []*Model, ignoreCols ...string) {
	for _, model := range models {
		bd.Save(model, ignoreCols...)
	}
}

// SaveBatchNotAssignId 非自动主键赋值批量插入方式
// 插入同一个表的多条数据，此时如果数据库支持批量插入，那么会进行批量插入，
// 但是这样每条记录就无法被自动赋予id值。
// 如果数据库不支持批量插入，那么就会一条一条插入。
func (bd *BaseDao[Model]) SaveBatchNotAssignId(models []*Model, forceUpdateCols ...string) {
	if len(models) > 0 {
		engine := GetDataSourceById(bd.GetDataSourceId())
		forceUpdateCols = append(forceUpdateCols, base.IdColumnName)
		omitCols := toolkit.GetZeroFieldNames4CustomStrategy(models[0],
			engine.GetColumnMapper().Obj2Table, forceUpdateCols...)
		_, err := engine.Omit(omitCols...).Insert(&models)
		if err != nil {
			base.Logger.Error("批量新增失败: %v", err)
			panic(errors.New("批量新增失败"))
		}
	}
}

// ================================= 删除 =================================

func (bd *BaseDao[Model]) Delete(wrapper *wrappers.Wrapper[Model]) {
	if wrapper == nil {
		// 条件为空，就会全部删除，高危操作，不允许
		// 如果真的想全部删除，可以传入空条件 wrappers.Query[Model]()
		return
	}
	engine := GetDataSourceById(bd.GetDataSourceId())
	_, err := engine.Where(wrapper.GetCond()).Delete(new(Model))
	if err != nil {
		base.Logger.Error("删除失败: %v", err)
		panic(errors.New("删除失败"))
	}
}

func (bd *BaseDao[Model]) DeleteById(id any) {
	engine := GetDataSourceById(bd.GetDataSourceId())
	_, err := engine.ID(id).Delete(new(Model))
	if err != nil {
		base.Logger.Error("根据主键删除记录失败: %v", err)
		panic(errors.New("根据主键删除记录失败"))
	}
}

func (bd *BaseDao[Model]) DeleteBatchIds(ids ...any) {
	engine := GetDataSourceById(bd.GetDataSourceId())
	_, err := engine.In(base.IdColumnName, ids...).
		NoAutoCondition(). // model 不自动生成where条件（禁用自动根据结构体中的值来生成条件）
		Delete(new(Model))
	if err != nil {
		base.Logger.Error("根据主键批量删除失败: %v", err)
		panic(errors.New("根据主键批量删除失败"))
	}
}

func (bd *BaseDao[Model]) SaveOrUpdate(model *Model, forceUpdateCols ...string) {
	if model == nil {
		return
	}
	id := Model.GetId(*model)
	if id == 0 {
		bd.Save(model, forceUpdateCols...)
	} else {
		bd.UpdateById(model, forceUpdateCols...)
	}
}

// ================================= 修改 =================================

func (bd *BaseDao[Model]) UpdateById(model *Model, forceUpdateCols ...string) {
	// 通过泛型类型和泛型实例获取id，这在java中要简单很多，直接model.GetId()即可
	// 这里可以通过泛型参数Model.GetId，也可以使用base.Domain.GetId，注意：参数需要传实例对象或实例对象的指针，具体传啥需要看定义的接收者是对象还是指针
	id := base.Domain.GetId(*model)
	engine := GetDataSourceById(bd.GetDataSourceId())
	_, err := engine.ID(id).
		// Update会自动从user结构体中提取非0和非nil得值作为需要更新的内容
		// 通过添加Cols函数指定需要更新结构体中的哪些值，未指定的将不更新，指定了的即使为0也会更新
		Cols(forceUpdateCols...).
		Update(model)
	if err != nil {
		base.Logger.Error("根据主键修改失败: %v", err)
		panic(errors.New("根据主键修改失败"))
	}
}

func (bd *BaseDao[Model]) Update(model *Model, wrapper *wrappers.Wrapper[Model], forceUpdateCols ...string) {
	if model == nil && wrapper == nil {
		return
	}
	engine := GetDataSourceById(bd.GetDataSourceId())
	_, err := engine.Cols(forceUpdateCols...).Where(wrapper.GetCond()).Update(model)
	if err != nil {
		base.Logger.Error("根据条件修改失败: %v", err)
		panic(errors.New("根据条件修改失败"))
	}
}

func (bd *BaseDao[Model]) UpdateMap(updateMap map[string]any, wrapper *wrappers.Wrapper[Model]) {
	if updateMap == nil && wrapper == nil {
		return
	}
	engine := GetDataSourceById(bd.GetDataSourceId())
	_, err := engine.Table(new(Model)).Where(wrapper.GetCond()).Update(updateMap)
	if err != nil {
		base.Logger.Error("根据条件修改失败: %v", err)
		panic(errors.New("根据条件修改失败"))
	}
}

// ================================= 查询 =================================

func getDefaultAndCond4Select(model base.Domain) builder.Cond {
	cond := base.Domain.DefaultAndCond4Select(model)
	if cond == nil {
		cond = builder.NewCond()
	}
	return cond
}

func (bd *BaseDao[Model]) SelectById(id int64) *Model {
	if id == 0 {
		return nil
	}
	model := new(Model)
	engine := GetDataSourceById(bd.GetDataSourceId())
	_, err := engine.ID(id).Where(getDefaultAndCond4Select(*model)).Get(model)
	if err != nil {
		return nil
	}
	if toolkit.IsNil(*model) {
		// 判断零值
		return nil
	}
	return model
}

func (bd *BaseDao[Model]) SelectCount(wrapper *wrappers.Wrapper[Model]) int64 {
	if wrapper == nil {
		wrapper = wrappers.Query[Model]()
	}
	engine := GetDataSourceById(bd.GetDataSourceId())
	count, err := engine.Where(wrapper.GetCond().And(getDefaultAndCond4Select(*new(Model)))).Count(new(Model))
	if count < 1 || err != nil {
		return 0
	}
	return count
}

func (bd *BaseDao[Model]) SelectList(wrapper *wrappers.Wrapper[Model]) []*Model {
	models := make([]*Model, 0)
	if wrapper == nil {
		wrapper = wrappers.Query[Model]()
	}
	engine := GetDataSourceById(bd.GetDataSourceId())
	wrapper.PutCond(getDefaultAndCond4Select(*new(Model)))
	err := engine.SQL(wrapper.GetBuilder(engine)).Find(&models)
	if err != nil {
		base.Logger.Error("根据条件查询models失败: %v", err)
	}
	return models
}

func (bd *BaseDao[Model]) SelectOne(wrapper *wrappers.Wrapper[Model]) *Model {
	model := new(Model)
	if wrapper == nil {
		wrapper = wrappers.Query[Model]()
	}
	engine := GetDataSourceById(bd.GetDataSourceId())
	wrapper.PutCond(getDefaultAndCond4Select(*new(Model)))
	_, err := engine.SQL(wrapper.GetBuilder(engine)).Get(model)
	if err != nil {
		return nil
	}
	if toolkit.IsNil(*model) {
		// 判断零值
		return nil
	}
	return model
}

func (bd *BaseDao[Model]) SelectMaps(wrapper *wrappers.Wrapper[Model]) []map[string]any {
	if wrapper == nil {
		wrapper = wrappers.Query[Model]()
	}
	engine := GetDataSourceById(bd.GetDataSourceId())
	wrapper.PutCond(getDefaultAndCond4Select(*new(Model)))
	results, err := engine.QueryInterface(wrapper.GetBuilder(engine))
	if err != nil {
		base.Logger.Error("根据条件查询maps失败: %v", err)
	}
	return results
}

func (bd *BaseDao[Model]) SelectPage(page IPage[Model], wrapper *wrappers.Wrapper[Model]) IPage[Model] {
	if page == nil {
		page = NewDefaultPage[Model]()
	}
	if wrapper == nil {
		wrapper = wrappers.Query[Model]()
	}
	start := page.Start()
	limit := page.Limit()
	models := make([]*Model, 0, limit)
	engine := GetDataSourceById(bd.GetDataSourceId())

	count, err := engine.
		Where(wrapper.GetCond().And(getDefaultAndCond4Select(*new(Model)))).
		Count(new(Model))
	if count <= 0 || err != nil {
		return page
	}
	page.SetTotal(count)

	err = engine.Cols(wrapper.GetSelect()...).
		Where(wrapper.GetCond().And(getDefaultAndCond4Select(*new(Model)))).
		Limit(limit, start).Find(&models)
	if err != nil {
		return page
	}
	page.SetRecords(models)
	return page
}
