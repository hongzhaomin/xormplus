package xormplus

import (
	"gitee.com/hongzhaomin/xormplus/base"
	"time"
	"xorm.io/builder"
)

var _ base.Domain = (*Entity)(nil)

// Entity 所有实体的父类，子类可以继承此model类，但必须使用`xorm:"extends"`标记在匿名成员结构体或者非匿名成员结构体之上
type Entity struct {
	// autoincr 必须添加才会填充主键
	Id    int64 `xorm:"BIGINT(20) notnull pk autoincr COMMENT('主键id')"`
	Valid byte  `xorm:"tinyint(1) DEFAULT 1 COMMENT('1 可用 0 不可用')"`
	// 在数据插入到数据库时自动将对应的字段设置为当前时间
	// 对应的字段可以为time.Time或者自定义的time.Time或者int,int64等int类型
	CreateTime time.Time `xorm:"created"`
	// 记录插入或每次记录更新时自动更新数据库中的标记字段为当前时间
	// 对应的字段可以为time.Time或者自定义的time.Time或者int,int64等int类型
	UpdateTime time.Time `xorm:"updated"`
}

func (en Entity) GetId() int64 {
	return en.Id
}

func (en Entity) IsBuiltin() bool {
	return true
}

func (en Entity) DefaultAndCond4Select() builder.Cond {
	return builder.Eq{"valid": 1}
}
